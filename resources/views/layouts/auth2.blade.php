<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'POS') }}</title>

    @include('layouts.partials.css')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    @inject('request', 'Illuminate\Http\Request')
    @if (session('status') && session('status.success'))
    <input type="hidden" id="status_span" data-status="{{ session('status.success') }}" data-msg="{{ session('status.msg') }}">
    @endif
    <div class="container-fluid">
        <div class="row eq-height-row">
            <div class="col-md-5 col-sm-5 left-col">
                <!-- <strong class="text-danger fw-bold" style="font-size: 60px;">Adva</strong><strong class="text-white fw-bold" style="font-size: 60px;">POS</strong> -->
                <div class="left-col-content login-header">

                    <div style="margin-top: 70%;">

                        <!-- <a href="/">
                            @if(file_exists(public_path('uploads/logo.png')))
                            <img src="/uploads/logo.png" class="img-rounded" alt="Logo" width="150">
                            @else
                            
                            @endif
                        </a> -->
                        <br />
                        <!-- @if(!empty(config('constants.app_title')))
                        <strong class="text-secondary fw-bold" style="font-size: 30px;">Kuza</strong>&nbsp;<strong class="text-white fw-bold" style="font-size: 30px;">Busi</strong><strong class="text-secondary fw-bold" style="font-size: 30px;">ness</strong>
                        @endif -->
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12 right-col eq-height-col">
                <div class="row">
                    <div class="col-md-3 col-xs-4" style="text-align: left;">
                        <select class="form-control input-sm" id="change_lang" style="margin: 10px;">
                            @foreach(config('constants.langs') as $key => $val)
                            <option value="{{$key}}" @if( (empty(request()->lang) && config('app.locale') == $key)
                                || request()->lang == $key)
                                selected
                                @endif
                                >
                                {{$val['full_name']}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-9 col-xs-8" style="text-align: right;padding-top: 10px;">
                        @if(!($request->segment(1) == 'business' && $request->segment(2) == 'register'))
                        <!-- Register Url -->
                        <!-- @if(config('constants.allow_registration'))
                        <a href="{{ route('business.getRegister') }}@if(!empty(request()->lang)){{'?lang=' . request()->lang}} @endif" class="btn bg-maroon btn-flat"><b>
                            {{ __('business.not_yet_registered')}}</b> {{ __('business.register_now') }}</a> -->
                        <!-- pricing url -->
                        @if(Route::has('pricing') && config('app.env') != 'demo' && $request->segment(1) != 'pricing')
                        &nbsp; <a href="{{ action([\Modules\Superadmin\Http\Controllers\PricingController::class, 'index']) }}">@lang('superadmin::lang.pricing')</a>
                        @endif
                        @endif
                        <!-- @endif -->
                        @if($request->segment(1) != 'login')


                        &nbsp; &nbsp;<span class="text-white">{{ __('business.already_registered')}} </span><a href="{{ action([\App\Http\Controllers\Auth\LoginController::class, 'login']) }}@if(!empty(request()->lang)){{'?lang=' . request()->lang}} @endif">{{ __('business.sign_in') }}</a>

                        @endif
                    </div>
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    @include('layouts.partials.javascripts')

    <!-- Scripts -->
    <script src="{{ asset('js/login.js?v=' . $asset_v) }}"></script>

    @yield('javascript')

    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2_register').select2();

            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>
<!-- Footer. -->
<footer class="text-white text-lg-start" style="background-color: #708090;">
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row mt-4">
            <!--Grid column-->
            <div class="col-lg-4 col-md-12 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-4">About AdvaPOS</h5>

                <p>
                    AdvaPOS is the best point-of-sale systems for shops, supermarket, grocery, and stores.
                    The system track inventory at real time, as well as it
                    provide thorough sales reporting, having good receipt options and are easy to use.
                </p>

                <p>
                    AdvaPOS deal with huge amounts of inventory data and rapid turnover of that inventory,
                    helping to track how much of each unit you sell and how much you have shelved.
                    You might also want this to sync across several locations if you have more than one store.
                </p>

                <div class="mt-4">
                    <!-- Facebook -->
                    <a type="button" class="btn btn-floating btn-danger btn-lg"><i class="fab fa-facebook-f"></i></a>
                    <!-- Instagram -->
                    <a type="button" class="btn btn-floating btn-warning btn-lg"><i class="fab fa-instagram"></i></a>
                    <!-- Twitter -->
                    <a type="button" class="btn btn-floating btn-info btn-lg"><i class="fab fa-twitter"></i></a>
                    <!-- Google + -->
                    <a type="button" class="btn btn-floating btn-success btn-lg"><i class="fab fa-google-plus-g"></i></a>
                </div>
            </div>
            <!--Grid column-->
            <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-4 pb-1">Sales | Services | Support</h5>
                <ul class="fa-ul" style="margin-left: 1.65em;">
                    <li class="mb-3">
                        <span class="fa-li"><i class="fas fa-home"></i></span><span class="ms-2">Dar Es Salaam, Tanzania HQ</span>
                    </li>
                    <li class="mb-2">
                        <span class="fa-li"><i class="fas fa-building"></i></span><span class="ms-0">Nssf, Mwl Nyerere Pension Tower 3<sup>rd</sup> Floor</span>
                    </li>
                    <li class="mb-2">
                        <span class="fa-li"><i class="fas fa-envelope"></i></span><span class="ms-0">info@advatech.co.tz | efd@advatechtz.com</span>
                    </li>
                    <li class="mb-2">
                        <span class="fa-li"><i class="fas fa-fax"></i></span><span class="ms-0">+255 658 956 525 | +255 755 643 474</span>
                    </li>
                </ul>
            </div>

            <!--Grid column-->
            <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-4 text-dark">Opening hours</h5>

                <table class="table text-center text-white">
                    <tbody class="font-weight-normal">
                        <tr>
                            <td>Mon - Fri:</td>
                            <td>8:00am - 17:00pm</td>
                        </tr>
                        <tr>
                            <td>Saturday:</td>
                            <td>8:30am - 13:00pm</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.2);">
        © {{date('Y')}} Copyright:
        <a class="text-white" href="#">AdvaTech Tanzania. All rights reserved</a>
    </div>
    <!-- Copyright -->
</footer>

<!-- End of .container -->

</html>