<!-- Main Footer -->
  <footer class="no-print text-center text-info">
    <!-- To the right -->
    <!-- <div class="pull-right hidden-xs">
      Anything you want
    </div> -->
    <!-- Default to the left -->
    <small class="text-danger text-center">
    	{{ config('app.name', 'ultimatePOS') }} - V1.0 | Copyright &copy; {{ date('Y') }} All rights reserved.
    </small>
</footer>